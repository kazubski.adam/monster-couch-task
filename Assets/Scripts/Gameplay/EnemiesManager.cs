using UnityEngine;
using Object = UnityEngine.Object;

namespace Gameplay
{
    public class EnemiesManager
    {
        private AgentHandler[] _enemies;

        public void Prepare (SpawnSettings spawnSettings, AgentSettings agentSettings,
                             Vector2 cameraMaxPosition) {
            _enemies = new AgentHandler[spawnSettings.EnemiesCount];
            for (var i = 0; i < spawnSettings.EnemiesCount; i++) {
                _enemies[i] = agentSettings.NewAgent;
                _enemies[i].transform.position = RandomSpawnPosition();
                _enemies[i].SwitchTo(true);

                Vector3 RandomSpawnPosition () {
                    var newPosition = i % 3 == 0 ? GetHorizontal() : GetVertical();
                    var direction = -newPosition.normalized;
                    var distance = Vector3.Distance(newPosition, Vector3.zero);
                    var maxDistance = distance - spawnSettings.MinSpawnDistance;
                    var distanceMultiplier = Random.Range(0f, 1f);
                    distanceMultiplier = Mathf.Pow(distanceMultiplier, 2);

                    return newPosition + direction * (maxDistance * distanceMultiplier);
                }

                Vector3 GetHorizontal () {
                    var x = Random.Range(0, 2) == 0 ? -cameraMaxPosition.x : cameraMaxPosition.x;
                    var y = Random.Range(-cameraMaxPosition.y, cameraMaxPosition.y);

                    return new Vector3(x, y, 0);
                }

                Vector3 GetVertical () {
                    var x = Random.Range(-cameraMaxPosition.x, cameraMaxPosition.x);
                    var y = Random.Range(0, 2) == 0 ? -cameraMaxPosition.y : cameraMaxPosition.y;

                    return new Vector3(x, y, 0);
                }
            }
        }

        public void Clear () {
            for (var i = 0; i < _enemies.Length; i++) Object.Destroy(_enemies[i].gameObject);
        }

        public void OnFixedUpdate (Vector3 playerPosition, Vector2 movementDirection,
                                   AgentSettings agentSettings, ChaseSettings chaseSettings,
                                   Vector2 cameraMaxPosition) {
            for (var i = 0; i < _enemies.Length; i++) {
                var enemy = _enemies[i];
                var enemyPosition = enemy.transform.position;
                var distance = Vector3.Distance(playerPosition, enemyPosition);

                if (distance > chaseSettings.MinChaseDistance) {
                    enemy.DecelerateWithMinSpeed(playerPosition, agentSettings, cameraMaxPosition);
                    continue;
                }

                var direction = (enemyPosition - playerPosition).normalized;
                var positionOffset = direction * chaseSettings.WallCheckDistance;
                var checkPosition = enemyPosition + positionOffset;
                var isPositionValid = IsPositionValid();

                if (isPositionValid) {
                    enemy.Accelerate(direction, agentSettings, cameraMaxPosition);
                    continue;
                }

                var validPosition = GetValidPosition();
                var validDirection = (validPosition - enemyPosition).normalized;

                enemy.Accelerate(validDirection, agentSettings, cameraMaxPosition);

                Vector3 GetValidPosition () {
                    var startAngle = Vector3.SignedAngle(movementDirection, direction, Vector3.up);
                    var angleOffset = 0f;
                    var multiplier = startAngle >= 0 ? 1 : -1;
                    var saveCounter = chaseSettings.SearchForValidPositionRandomSaveCounter;

                    while (!IsPositionValid()) {
                        if (saveCounter < 0) break;

                        checkPosition = GetPoint();
                        angleOffset += chaseSettings.SearchForValidPositionAngle;
                        saveCounter--;
                    }
                    
                    return checkPosition;

                    Vector3 GetPoint () {
                        var angle = Vector3.forward * (startAngle + angleOffset * multiplier);
                        return enemyPosition + Quaternion.Euler(angle) * positionOffset;
                    }
                }

                bool IsPositionValid () => checkPosition.x > -cameraMaxPosition.x
                                        && checkPosition.x < cameraMaxPosition.x
                                        && checkPosition.y > -cameraMaxPosition.y
                                        && checkPosition.y < cameraMaxPosition.y;

                bool IsOnLeft (Vector3 vector, Vector3 point) =>
                    -vector.x * point.y + vector.y * point.x < 0;
            }
        }
    }
}
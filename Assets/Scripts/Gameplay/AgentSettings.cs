﻿using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(menuName = "Scriptable Objects/Agent Settings", 
                     fileName = "New Agent Settings", order = 0)]
    public class AgentSettings : ScriptableObject
    {
        [SerializeField] private float _minMovementMagnitude;
        [SerializeField] private float _maxMovementMagnitude;
        [SerializeField] private float _accelerationStep;
        [SerializeField] private float _decelerationMultiplier;
        [SerializeField] private float _minBounceOffVelocity;
        [SerializeField] private float _bounceOffMultiplier;
        [SerializeField] private AgentHandler _prefab;

        public float MinMovementMagnitude => _minMovementMagnitude;
        public float MaxMovementMagnitude => _maxMovementMagnitude;
        public float AccelerationStep => _accelerationStep;
        public float DecelerationMultiplier => _decelerationMultiplier;
        public float MinBounceOffVelocity => _minBounceOffVelocity;
        public float BounceOffMultiplier => _bounceOffMultiplier;
        public AgentHandler NewAgent => Instantiate(_prefab);
    }
}
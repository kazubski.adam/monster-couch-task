﻿using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(menuName = "Scriptable Objects/Chase Settings", 
                     fileName = "New Chase Settings", order = 0)]
    public class ChaseSettings : ScriptableObject
    {
        [SerializeField] private float _minChaseDistance;
        [SerializeField] private float _wallCheckDistance;
        [SerializeField] private float _searchForValidPositionAngle;
        [SerializeField] private int _searchForValidPositionMinSaveCounter;
        [SerializeField] private int _searchForValidPositionMaxSaveCounter;

        public float MinChaseDistance => _minChaseDistance;
        public float WallCheckDistance => _wallCheckDistance;
        public float SearchForValidPositionAngle => _searchForValidPositionAngle;
        public int SearchForValidPositionRandomSaveCounter =>
            Random.Range(_searchForValidPositionMinSaveCounter, _searchForValidPositionMaxSaveCounter);
    }
}
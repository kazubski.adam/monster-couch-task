﻿using UnityEngine;

namespace Gameplay
{
    [CreateAssetMenu(menuName = "Scriptable Objects/Spawn Settings", 
                     fileName = "New Spawn Settings", order = 0)]
    public class SpawnSettings : ScriptableObject
    {
        [SerializeField] private int _enemiesCount;
        [SerializeField] private float _minSpawnDistance;

        public int EnemiesCount => _enemiesCount;
        public float MinSpawnDistance => _minSpawnDistance;
    }
}
using UnityEngine;
using Cache = Essentials.Cache;

namespace Gameplay
{
    public class AgentHandler : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D _rigidbody;
        [SerializeField] private SpriteRenderer _spriteRenderer;

        public Rigidbody2D Rigidbody => _rigidbody;
        public SpriteRenderer SpriteRenderer => _spriteRenderer;

        private bool _isEnabled;

        public bool SwitchTo (bool switchTo) => _isEnabled = switchTo;
        
        public void Accelerate (Vector2 direction, AgentSettings agentSettings,
                                Vector2 cameraMaxPosition) {
            if (!_isEnabled) return;
            
            var rawVelocity = _rigidbody.velocity + direction * agentSettings.AccelerationStep;
            var newVelocity = GetClampedVelocity(rawVelocity, agentSettings, cameraMaxPosition);
            _rigidbody.velocity = newVelocity;
            UpdateRotation();
        }

        public void DecelerateWithMinSpeed (Vector3 playerPosition, AgentSettings agentSettings,
                                            Vector2 cameraMaxPosition) {
            if (!_isEnabled) return;

            var rawVelocity = Rigidbody.velocity * agentSettings.DecelerationMultiplier;
            var rawMagnitude = rawVelocity.magnitude;
            
            if (rawMagnitude < agentSettings.MinMovementMagnitude) {
                var direction = rawVelocity.normalized;
                if (direction == Vector2.zero) {
                    direction = (transform.position - playerPosition).normalized;
                }
                rawVelocity = direction * agentSettings.MinMovementMagnitude;
            }
            
            var newVelocity = GetClampedVelocity(rawVelocity, agentSettings, cameraMaxPosition);
            _rigidbody.velocity = newVelocity;
            UpdateRotation();
        }
        
        public void Decelerate (AgentSettings agentSettings, Vector2 cameraMaxPosition) {
            if (!_isEnabled) return;

            var rawVelocity = Rigidbody.velocity * agentSettings.DecelerationMultiplier;
            var newVelocity = GetClampedVelocity(rawVelocity, agentSettings, cameraMaxPosition);
            _rigidbody.velocity = newVelocity;
            UpdateRotation();
        }

        private void UpdateRotation () {
            if (!_isEnabled) return;
            
            var movementDirection = _rigidbody.velocity.normalized;
            if (_rigidbody.velocity.normalized == Vector2.zero) return;
            var rotation = Quaternion.LookRotation(movementDirection, Vector3.forward);
            _rigidbody.MoveRotation(rotation);
        }

        private Vector2 GetClampedVelocity (Vector2 rawVelocity, AgentSettings agentSettings, 
                                            Vector2 cameraMaxPosition) {
            var clampedVelocity = Vector2.ClampMagnitude(rawVelocity, agentSettings.MaxMovementMagnitude);
            var currentPosition = (Vector2) transform.position;
            var newPosition = currentPosition + clampedVelocity * Cache.FixedDeltaTime;
            var saveCounter = 0;
            var isPositionValid = false;

            while (!isPositionValid) {
                if (saveCounter > 10) return -clampedVelocity;

                isPositionValid = true;

                if (newPosition.x < -cameraMaxPosition.x || newPosition.x > cameraMaxPosition.x) {
                    clampedVelocity.x = GetBounceOffVelocity(clampedVelocity.x);
                    newPosition = currentPosition + clampedVelocity * Cache.FixedDeltaTime;
                    isPositionValid = false;
                }

                if (newPosition.y < -cameraMaxPosition.y || newPosition.y > cameraMaxPosition.y) {
                    clampedVelocity.y = GetBounceOffVelocity(clampedVelocity.y);
                    newPosition = currentPosition + clampedVelocity * Cache.FixedDeltaTime;
                    isPositionValid = false;
                }

                saveCounter++;
            }

            return clampedVelocity;
        
            float GetBounceOffVelocity (float currentVelocity) {
                var bounceOffVelocity = -currentVelocity * agentSettings.BounceOffMultiplier;

                if (bounceOffVelocity >= 0 && bounceOffVelocity < agentSettings.MinBounceOffVelocity)
                    bounceOffVelocity = agentSettings.MinBounceOffVelocity;
                else if (bounceOffVelocity <= 0 && bounceOffVelocity > agentSettings.MinBounceOffVelocity)
                    bounceOffVelocity = -agentSettings.MinBounceOffVelocity;

                return bounceOffVelocity;
            }
        }
    }
}
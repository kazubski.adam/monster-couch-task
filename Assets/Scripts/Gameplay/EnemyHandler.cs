using UnityEngine;

namespace Gameplay
{
    public class EnemyHandler : AgentHandler
    {
        private void OnTriggerEnter2D (Collider2D col) {
            SwitchTo(false);
            Rigidbody.velocity = Vector2.zero;
            SpriteRenderer.color = Color.gray;
        }
    }
}
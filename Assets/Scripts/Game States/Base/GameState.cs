namespace Game_States.Base
{
    public abstract class GameState
    {
        protected readonly GameStates context; 
    
        public abstract GameState DefaultPreviousState { get; }

        private GameState () { }

        protected GameState (GameStates newContext) {
            context = newContext;
        }

        public abstract void OnGameStateEnter ();
        public abstract void OnGameStateExit ();
        public abstract void OnGameStateUpdate ();
        public abstract void OnGameStateFixedUpdate ();
    }
}
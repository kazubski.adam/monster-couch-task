using Game_States.In_Game;
using Game_States.Main_Menu;
using UnityEngine;

namespace Game_States.Base
{
    public class GameStates : MonoBehaviour
    {
        private MainMenu_UI _mainMenu_UI;

        public MainMenu mainMenu;
        public Settings.Settings settings;
        public InGame inGame;

        public static GameState CurrentGameState { get; private set; }

        private void Awake () {
            mainMenu = new MainMenu(this);
            settings = new Settings.Settings(this);
            inGame = new InGame(this);
        }

        private void Start () {
            CurrentGameState = mainMenu;
            CurrentGameState.OnGameStateEnter();
        }

        private void Update () => CurrentGameState.OnGameStateUpdate();
        private void FixedUpdate () => CurrentGameState.OnGameStateFixedUpdate();

        public void Enter (GameState newGameState) {
            CurrentGameState.OnGameStateExit();
            CurrentGameState = newGameState;
            CurrentGameState.OnGameStateEnter();
        }
    }
}

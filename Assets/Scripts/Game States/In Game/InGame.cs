using Game_States.Base;
using UnityEngine;

namespace Game_States.In_Game
{
    public class InGame : GameState
    {
        private readonly InGame_Logic _inGameLogic;
        private readonly InGame_UI _inGameUI;
    
        public override GameState DefaultPreviousState => context.mainMenu;

        public InGame (GameStates newContext) : base(newContext) {
            _inGameLogic = Object.FindObjectOfType<InGame_Logic>();
            _inGameUI = Object.FindObjectOfType<InGame_UI>();
            
            _inGameUI.BackButton.onClick.AddListener(() => context.Enter(context.mainMenu));
        }

        public override void OnGameStateEnter () {
            _inGameUI.gameObject.SetActive(true);
            _inGameLogic.Prepare();
        }

        public override void OnGameStateExit () {
            _inGameUI.gameObject.SetActive(false);
            _inGameLogic.Clear();
        }

        public override void OnGameStateUpdate () {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                _inGameLogic.Clear();
                context.Enter(context.mainMenu);
                return;
            }
        
            _inGameLogic.OnUpdate();
        }

        public override void OnGameStateFixedUpdate () => _inGameLogic.OnFixedUpdate();
    }
}
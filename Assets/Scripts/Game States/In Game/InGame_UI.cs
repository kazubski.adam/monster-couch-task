using UnityEngine;
using UnityEngine.UI;

namespace Game_States.In_Game
{
    public class InGame_UI : MonoBehaviour
    {
        [SerializeField] private Button _backButton;

        public Button BackButton => _backButton;

        private void Start () {
            GetComponent<Canvas>().enabled = true;
            gameObject.SetActive(false);
        }
    }
}
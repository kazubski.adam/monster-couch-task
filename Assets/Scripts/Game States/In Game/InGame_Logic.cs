using Essentials.Input;
using Gameplay;
using UnityEngine;
using Cache = Essentials.Cache;

namespace Game_States.In_Game
{
    public class InGame_Logic : MonoBehaviour, IInputProvider
    {
        [SerializeField] private SpawnSettings _spawnSettings;
        [SerializeField] private ChaseSettings _chaseSettings;
        [SerializeField] private AgentSettings _playerSettings;
        [SerializeField] private AgentSettings _enemiesSettings;
    
        private AgentHandler _playerHandler;
        private readonly EnemiesManager _enemiesManager = new();
    
        private Vector2 _cameraMaxPosition;
        private Vector2 _movementDirection;
        private bool _isOn;
        private bool _isMouseDown;
        private bool _isGettingInput;
    
        public int InputHandlerIndex { get; set; }

        private void Awake () => _cameraMaxPosition = GetZeroPlanePosition(Cache.ScreenSize);

        public void Prepare () {
            _playerHandler = _playerSettings.NewAgent;
            _playerHandler.SwitchTo(true);
            _enemiesManager.Prepare(_spawnSettings, _enemiesSettings, _cameraMaxPosition);
            _isMouseDown = false;
            _isOn = true;
        
            InputManager.RegisterProvider(this);
        }
    
        public void Clear () {
            Destroy(_playerHandler.gameObject);
            _enemiesManager.Clear();
            _isOn = false;
        
            InputManager.UnregisterProvider(this);
        }

        public void OnDown () => _isMouseDown = true;

        public void OnUp () => _isMouseDown = false;

        public void OnUpdate () {
            if (!_isOn) return;

            var newDirection = Vector2.zero;

            if (_isMouseDown) ManageMouse();
            else ManageKeyboard();

            void ManageMouse () {
                var zeroPlanePosition = GetZeroPlanePosition(InputManager.CurrentPointerPosition);
                newDirection = (zeroPlanePosition - _playerHandler.transform.position).normalized;

                _isGettingInput = true;
                newDirection.Normalize();
            }

            void ManageKeyboard () {
                var isUp = Input.GetKey(KeyCode.UpArrow);
                var isDown = Input.GetKey(KeyCode.DownArrow);
                var isRight = Input.GetKey(KeyCode.RightArrow);
                var isLeft = Input.GetKey(KeyCode.LeftArrow);

                newDirection.y = isUp ? isDown ? 0 : 1 : isDown ? -1 : 0;
                newDirection.x = isRight ? isLeft ? 0 : 1 : isLeft ? -1 : 0;

                if (newDirection == Vector2.zero) {
                    _isGettingInput = false;
                    return;
                }

                _isGettingInput = true;
                newDirection.Normalize();
            }

            _movementDirection = newDirection;
        }

        public void OnFixedUpdate () {
            if (!_isOn) return;

            if (_isGettingInput)
                _playerHandler.Accelerate(_movementDirection, _playerSettings, _cameraMaxPosition);
            else _playerHandler.Decelerate(_playerSettings, _cameraMaxPosition);
        
            _enemiesManager.OnFixedUpdate(_playerHandler.transform.position, _movementDirection,
                                          _enemiesSettings, _chaseSettings, _cameraMaxPosition);
        }

        public static Vector3 GetZeroPlanePosition (Vector2 screenPosition) {
            var ray = Cache.MainCamera.ScreenPointToRay(screenPosition);
            var hit = Physics.Raycast(ray, out var hitInfo, 400, 1 << InputManager.ZeroPlaneLayer);
        
            return !hit ? Vector3.zero : hitInfo.point;
        }
    }
}
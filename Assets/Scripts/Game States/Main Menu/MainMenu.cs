using Essentials;
using Essentials.Input;
using Extensions;
using Game_States.Base;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game_States.Main_Menu
{
    public class MainMenu : GameState, IInputProvider
    {
        public int InputHandlerIndex { get; set; }
    
        private readonly MainMenu_UI _mainMenuUI;
        private GameObject _currentlySelected;
    
        public override GameState DefaultPreviousState => null;

        public MainMenu (GameStates newContext) : base(newContext) {
            _mainMenuUI = Object.FindObjectOfType<MainMenu_UI>();
            _mainMenuUI.StartButton.onClick.AddListener(() => context.Enter(context.inGame));
            _mainMenuUI.SettingsButton.onClick.AddListener(() => context.Enter(context.settings));
            _mainMenuUI.ExitButton.onClick.AddListener(ExitGame);
        }

        public override void OnGameStateEnter () {
            _mainMenuUI.gameObject.SetActive(true);
            _currentlySelected = _mainMenuUI.StartButton.gameObject;
            ResetSelection();
        
            InputManager.RegisterProvider(this);
        }

        public override void OnGameStateExit () {
            _mainMenuUI.gameObject.SetActive(false);
        
            InputManager.UnregisterProvider(this);
        }

        public override void OnGameStateUpdate () {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                ExitGame();
                return;
            }

            var newObject = EventSystem.current.currentSelectedGameObject;
            if(newObject != null) _currentlySelected = newObject;
        }

        public override void OnGameStateFixedUpdate () { }

        private void ExitGame () => Application.Quit();

        public void OnDown () { }

        public void OnUp () {
            var currentObject = CustomInputModule.GetCurrentObject();

            if (currentObject == null) {
                ResetSelection();
                return;
            }

            if (currentObject.IsInLayerMask(InputManager.UILayer)) return;
        
            ResetSelection();
        }

        private void ResetSelection () => _currentlySelected.GetComponent<Button>().Select();
    }
}
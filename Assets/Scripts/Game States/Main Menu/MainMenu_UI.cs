using UnityEngine;
using UnityEngine.UI;

namespace Game_States.Main_Menu
{
    public class MainMenu_UI : MonoBehaviour
    {
        [SerializeField] private Button _startButton;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _exitButton;
    
        public Button StartButton => _startButton;
        public Button SettingsButton => _settingsButton;
        public Button ExitButton => _exitButton;

        private void Start () {
            GetComponent<Canvas>().enabled = true;
            gameObject.SetActive(false);
        }
    }
}
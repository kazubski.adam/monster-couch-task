using UnityEngine;
using UnityEngine.UI;

namespace Game_States.Settings
{
    public class Settings_UI : MonoBehaviour
    {
        [SerializeField] private Toggle _toggle1;
        [SerializeField] private Toggle _toggle2;
        [SerializeField] private Button _backButton;

        public Toggle Toggle1 => _toggle1;
        public Toggle Toggle2 => _toggle2;
        public Button BackButton => _backButton;

        private void Start () {
            GetComponent<Canvas>().enabled = true;
            gameObject.SetActive(false);
        }
    }
}
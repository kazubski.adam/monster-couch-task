using Essentials;
using Essentials.Input;
using Extensions;
using Game_States.Base;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game_States.Settings
{
    public class Settings : GameState, IInputProvider
    {
        public int InputHandlerIndex { get; set; }
    
        private readonly Settings_UI _settingsUI;
        private GameObject _currentlySelected;

        public override GameState DefaultPreviousState => context.mainMenu;

        public Settings (GameStates newContext) : base(newContext) {
            _settingsUI = Object.FindObjectOfType<Settings_UI>();
            _settingsUI.BackButton.onClick.AddListener(() => context.Enter(context.mainMenu));
        }

        public override void OnGameStateEnter () {
            _settingsUI.gameObject.SetActive(true);
            _currentlySelected = _settingsUI.Toggle1.gameObject;
            ResetSelection();
        
            InputManager.RegisterProvider(this);
        }

        public override void OnGameStateExit () {
            _settingsUI.gameObject.SetActive(false);
        
            InputManager.UnregisterProvider(this);
        }

        public override void OnGameStateUpdate () {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                context.Enter(context.mainMenu);
                return;
            }
        
            var newObject = EventSystem.current.currentSelectedGameObject;
            if(newObject != null) _currentlySelected = newObject;
        }

        public override void OnGameStateFixedUpdate () { }

        public void OnDown () { }

        public void OnUp () {
            var currentObject = CustomInputModule.GetCurrentObject();

            if (currentObject == null) {
                ResetSelection();
                return;
            }

            if (currentObject.IsInLayerMask(InputManager.UILayer)) return;
        
            ResetSelection();
        }

        private void ResetSelection () => _currentlySelected.GetComponent<Selectable>().Select();
    }
}
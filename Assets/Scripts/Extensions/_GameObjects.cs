namespace Extensions
{
    public static class _GameObjects
    {
        public static bool IsInLayerMask (this UnityEngine.GameObject gameObject, int layerMask) {
            if (gameObject == null) return false;
            return layerMask == gameObject.layer;
        }
    }
}
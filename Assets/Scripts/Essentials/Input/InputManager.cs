using System.Collections.Generic;
using Attributes;
using UnityEngine;

namespace Essentials.Input
{
    public class InputManager : MonoBehaviour
    {
        [Header("Prefabs")]
        [SerializeField] private GameObject _zeroPlanePrefab;

        [Header("Layers")]
        [SerializeField] [LayerIndex] private int _zeroPlaneLayer;
        [SerializeField] [LayerIndex] private int _uiLayer;

        public static int ZeroPlaneLayer => _Instance._zeroPlaneLayer;
        public static int UILayer => _Instance._uiLayer;

        public static Vector2 PreviousPointerPosition { get; private set; }
        public static Vector2 StartPointerPosition { get; private set; }
        public static Vector2 CurrentPointerPosition { get; private set; }

        private static InputManager _Instance { get; set; }
        private static InputState _CurrentInputState { get; set; }
        private static bool _isGettingInput;

        private static readonly List<IInputProvider> _inputProviders = new();

        public static void RegisterProvider (IInputProvider inputProvider) {
            if (inputProvider.InputHandlerIndex != 0) return;
            inputProvider.InputHandlerIndex = _inputProviders.Count + 1;
            _inputProviders.Add(inputProvider);
            _isGettingInput = _inputProviders.Count > 0;
        }

        public static void UnregisterProvider (IInputProvider inputProvider) {
            if (inputProvider.InputHandlerIndex == 0) return;
            _inputProviders.RemoveAt(inputProvider.InputHandlerIndex - 1);
            inputProvider.InputHandlerIndex = 0;
            _isGettingInput = _inputProviders.Count > 0;
        }

        private static void OnHandlersDown () {
            for (var i = 0; i < _inputProviders.Count; i++) _inputProviders[i].OnDown();
        }

        private static void OnHandlersUp () {
            for (var i = 0; i < _inputProviders.Count; i++) _inputProviders[i].OnUp();
        }

        private void Awake () {
            _Instance = this;

            Instantiate(_zeroPlanePrefab);
        }

        private void Update () {
            RefreshInput();
            switch (_CurrentInputState) {
                case InputState.Up:
                    if (_isGettingInput) OnDownEnter();

                    break;
                case InputState.Down:
                    if (!_isGettingInput) OnDownExit();

                    break;
            }

            void OnDownEnter () {
                _CurrentInputState = InputState.Down;
                StartPointerPosition = CurrentPointerPosition;

                OnHandlersDown();
            }

            void OnDownExit () {
                _CurrentInputState = InputState.Up;

                OnHandlersUp();
            }

            void RefreshInput () {
                #if UNITY_EDITOR
                _isGettingInput = UnityEngine.Input.GetMouseButton(0);

                if (!_isGettingInput) return;

                var rawPointerPosition = UnityEngine.Input.mousePosition;
                var horizontallyCorrect = rawPointerPosition.x > 0
                                       || rawPointerPosition.x < Cache.ScreenSize.x;

                var verticallyCorrect = rawPointerPosition.y > 0
                                     || rawPointerPosition.y < Cache.ScreenSize.y;

                if (!horizontallyCorrect || !verticallyCorrect) return;

                PreviousPointerPosition = CurrentPointerPosition;
                CurrentPointerPosition = rawPointerPosition;
                #else
                var touches = UnityEngine.Input.touches;
                var isTouched = touches.Length > 0;
                _isGettingInput = isTouched;

                if (!_isGettingInput) return;

                PreviousPointerPosition = CurrentPointerPosition;
                CurrentPointerPosition = touches[0].position;
                #endif
            }
        }
    }

    public enum InputState
    {
        Up, Down
    }
}
namespace Essentials.Input
{
    public interface IInputProvider
    {
        public void OnDown ();
        public void OnUp ();

        public int InputHandlerIndex { get; set; }
    }
}
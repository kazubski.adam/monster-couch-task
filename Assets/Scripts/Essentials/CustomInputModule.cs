using UnityEngine;
using UnityEngine.EventSystems;

namespace Essentials
{
    public class CustomInputModule : StandaloneInputModule
    {
        private static CustomInputModule _instance;
    
        protected override void Awake () {
            base.Awake();
        
            _instance = this;
        }

        private GameObject GameObjectUnderPointer(int pointerId)
        {
            var lastPointer = GetLastPointerEventData(pointerId);
            return lastPointer?.pointerCurrentRaycast.gameObject;
        }
 
        public static GameObject GetCurrentObject() => _instance.GameObjectUnderPointer(kMouseLeftId);
    }
}
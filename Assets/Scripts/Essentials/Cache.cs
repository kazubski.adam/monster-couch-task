using System.Collections;
using UnityEditor;
using UnityEngine;

namespace Essentials
{
    public class Cache : MonoBehaviour
    {
        private static Cache _instance;
    
        #if UNITY_EDITOR
        public static PlayModeStateChange CurrentPlayModeState { get; private set; }
        public static Vector2Int ScreenSize => MainCamera == null ? new Vector2Int(0, 0)
                                                   : new Vector2Int(MainCamera.pixelWidth,
                                                                    MainCamera.pixelHeight);

        public static Vector2Int ScreenCenter => ScreenSize / 2;
        public static Vector2 ScreenOffset => new((float) Screen.width / 2, (float) Screen.height / 2);
        #else
    public static Vector2Int ScreenSize { get; private set; }
    public static Vector2Int ScreenCenter { get; private set; }
    public static Vector2Int ScreenOffset { get; private set; }
        #endif
    
        public static Camera MainCamera { get; private set; }
        public static float FixedDeltaTime { get; private set; }
        public static float DeltaTime { get; private set; }

        private void Awake () {
            _instance = this;
            MainCamera = Camera.main;
        
            #if !UNITY_EDITOR
        ScreenSize = new Vector2Int(MainCamera.pixelWidth, MainCamera.pixelHeight);
        ScreenCenter = ScreenSize / 2;
        ScreenOffset = new Vector2Int(Screen.width, Screen.height) / 2;
            #endif
        }

        public void Update () {
            FixedDeltaTime = Time.fixedDeltaTime;
            DeltaTime = Time.deltaTime;
        }

        private Coroutine BaseStartCoroutine (IEnumerator coroutine) => base.StartCoroutine(coroutine);
        private void BaseStopCoroutine (Coroutine coroutine) => base.StopCoroutine(coroutine);
    
        public new static Coroutine StartCoroutine (IEnumerator enumerator) => 
            _instance.BaseStartCoroutine(enumerator);

        public new static void StopCoroutine (Coroutine coroutine) =>
            _instance.BaseStopCoroutine(coroutine);
    
        #if UNITY_EDITOR
        public void OnValidate () => EditorApplication.playModeStateChanged += (state) =>
        {
            CurrentPlayModeState = state;
        };
        #endif
    }
}
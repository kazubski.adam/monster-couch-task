using System.Collections.Generic;
using Attributes;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomPropertyDrawer(typeof(LayerIndexAttribute))]
    public class LayerIndexAttributeDrawer : PropertyDrawer
    {
        private int _currentIndex;

        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
            var names = new List<string>();
            var indexes = new List<int>();

            for (var i = 0; i < 32; i++) {
                var layerName = LayerMask.LayerToName(i);

                if (property.intValue == i) _currentIndex = names.Count;
            
                if (layerName == "") continue;

                names.Add(layerName);
                indexes.Add(i);
            }

            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginProperty(position, label, property);
            _currentIndex = EditorGUI.Popup(position, label.text, _currentIndex, names.ToArray());
            if (EditorGUI.EndChangeCheck()) property.intValue = indexes[_currentIndex];
            EditorGUI.EndProperty();
        }
    }
}